// console.log("Hello");

let number = Number(prompt("Give me a number: "));
    console.log("The provided number is " + number);

    for (let count=number; count>=0; count--) {

        if (count % 10 == 0 ) {
            console.log("The number is divisible by 10." )    
            

        }else if (count % 5 == 0 ){
            console.log("The number is divisible by 5. ")
        
            console.log(count);
            
        }
        else if(count <= 50){
            console.log("Teminated");
            break;
        }
    }


    /*
        - Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
        - How this For Loop works:
            1. The loop will start at 0 for the the value of "count".
            2. It will check if "count" is less than the or equal to 20.
            3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
            4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
            5. If the value of count is not equal to 0, the console will print the value of "count".
            6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
            7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
            8. The value of "count" will be incremented by 1 (e.g. count = 1)
            9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] > 10) is true, the loop will stop due to the "break" statement
    */
    /*
    for (let count = 0; count <= 20; count++) {

        // if remainder is equal to 0
        if (count % 2 === 0) {

            // Tells the code to continue to the next iteration of the loop
            // This ignores all statements located after the continue statement;
            continue;

        }

        // The current value of number is printed out if the remainder is not equal to 0
        console.log("Continue and Break: " + count);

        // If the current value of count is greater than 10
        if (count > 10) {

            // Tells the code to terminate/stop the loop even if the expression/condition of the loop defines that it should execute so long as the value of count is less than or equal to 20
            // number values after 10 will no longer be printed
            break;

        }
    }
    */

    let name = "alexandro";

    /*
        - Creates a loop that will iterate based on the length of the string
        - How this For Loop works:
            1. The loop will start at 0 for the the value of "i"
            2. It will check if "i" is less than the length of name (e.g. 0)
            3. The if statement will check if the value of name[i] converted to a lowercase letter a (e.g. name[0] = a)
            4. If the expression/condition of the if statement is true the loop will continue to the next iteration.
            5. If the value of name[i] is not equal to a, the second if statement will be evaluated
            6. The second if statement will check if the value of name[i] converted to a lowercase letter d (e.g. name[0] = d)
            7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
            8. The value of "i" will be incremented by 1 (e.g. i = 1)
            9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement (e.g. name[0] = d) is true, the loop will stop due to the "break" statement
    */
    for (let i = 0; i < name.length; i++) {
        
        // The current letter is printed out based on it's index
        console.log(name[i]);

        // If the vowel is equal to a, continue to the next iteration of the loop
        if (name[i].toLowerCase() === "a") {
            console.log("Continue to the next iteration");
            continue;
        }

        // If the current letter is equal to d, stop the loop
        if (name[i] == "d") {
            break;
        }

    }

